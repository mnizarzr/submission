import 'dart:core';

class Course {
  String title;
  String duration;
  int totalStudents;
  String shortDescription;
  String imageUrl;

  Course(this.title, this.duration, this.totalStudents, this.shortDescription,
      this.imageUrl);

  static List<Course> get courses => [
        Course(
            "Belajar Membuat Aplikasi Flutter untuk Pemula",
            "40 Jam",
            8103,
            "Buat aplikasi pertamamu dengan Flutter. Pelajari konsep dasar layouting dengan widget dan menjalankan aplikasi di platform mobile & web.",
            "https://pbs.twimg.com/profile_images/1376574021203062796/Pu-eI87V_400x400.jpg"),
        Course(
            "Belajar Membuat Aplikasi Android untuk Pemula",
            "40 Jam",
            96344,
            "Buat aplikasi pertamamu pada Android Studio dengan mempelajari dasar Activity, Intent, View & ViewGroup, Style & Theme sampai RecyclerView.",
            "https://pbs.twimg.com/media/ELmBFEXXUAQwT1T.png"),
        Course(
            "Belajar Fundamental Aplikasi Android",
            "150 Jam",
            35444,
            "Pelajari skill Android dengan kurikulum terlengkap yang dibutuhkan perusahaan. Mulai dari UI/UX, background process, API sampai database.",
            "https://pbs.twimg.com/media/C6bgyyjU0AY9OUa.jpg")
      ];
}
