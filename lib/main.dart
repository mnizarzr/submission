import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:submission/app_theme_data.dart';
import 'package:submission/root.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(const Application());
}

// Referensi desain https://dribbble.com/shots/17968496-Online-Course-App
class Application extends StatelessWidget {
  const Application({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Online Course App',
      debugShowCheckedModeBanner: false,
      theme: AppThemeData.applicationThemeData,
      home: const RootPage(),
    );
  }
}
