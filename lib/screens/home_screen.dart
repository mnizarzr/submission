import 'package:flutter/material.dart';
import 'package:submission/app_theme_data.dart';
import 'package:submission/model/course.dart';
import 'package:submission/screens/course_detail_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var size = MediaQuery.of(context).size;
    var courses = Course.courses;

    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const UserHeader(),
              const SizedBox(
                height: 30,
              ),
              const Search(),
              const SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.all(defaultPadding),
                child: Row(
                  children: [
                    Text(
                      "Ready for Next?",
                      style: theme.textTheme.headline6
                          ?.copyWith(fontSize: 24.0, color: darkPurple),
                    ),
                    const Spacer(),
                    TextButton(
                        onPressed: () {
                          return;
                        },
                        child: const Text("See All"))
                  ],
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: defaultPadding),
                      child: CourseCard(course: courses[0]),
                    ),
                    CourseCard(course: courses[1]),
                    CourseCard(course: courses[2])
                  ],
                ),
              )
              // const CourseList()
            ],
          ),
        ),
      ),
    );
  }
}

class CourseCard extends StatelessWidget {
  const CourseCard({Key? key, required this.course}) : super(key: key);

  final Course course;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => CourseDetailScreen(course: course)));
      },
      child: Container(
        decoration: const BoxDecoration(
            borderRadius: BorderRadius.vertical(top: Radius.circular(20.0))),
        margin: const EdgeInsets.only(right: 16.0),
        width: size.width / 2,
        // height: 400,
        child: Stack(
          children: [
            Container(
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
              height: 200,
              child: ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(20.0)),
                child: Image.network(
                  course.imageUrl,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned.fill(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withAlpha(23),
                            offset: const Offset(0, 10),
                            blurRadius: 50)
                      ],
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(20.0),
                        bottomLeft: Radius.circular(20.0),
                        bottomRight: Radius.circular(20.0),
                      )),
                  padding: const EdgeInsets.all(defaultPadding),
                  child: Text(course.title),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class Search extends StatelessWidget {
  const Search({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var size = MediaQuery.of(context).size;
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
      margin: const EdgeInsets.symmetric(horizontal: defaultPadding),
      height: 80,
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Icon(
          Icons.search_outlined,
          size: 30,
          color: theme.colorScheme.secondary,
        ),
        Expanded(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
          child: TextField(
            decoration: InputDecoration.collapsed(
                hintText: "Search",
                hintStyle: TextStyle(color: theme.colorScheme.primary)),
          ),
        )),
        Icon(
          Icons.settings_input_component_outlined,
          size: 30,
          color: theme.colorScheme.secondary,
        )
      ]),
      decoration: BoxDecoration(
          border: Border.all(width: 1.5, color: theme.colorScheme.secondary),
          borderRadius: const BorderRadius.all(Radius.circular(50))),
    );
  }
}

class UserHeader extends StatelessWidget {
  const UserHeader({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return Padding(
      padding: const EdgeInsets.all(defaultPadding),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const CircleAvatar(
            radius: 30,
            backgroundImage: NetworkImage(
                "https://pbs.twimg.com/profile_images/1001443343556001798/Sn9Lwx20_400x400.jpg"),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Welcome back,",
                      style:
                          theme.textTheme.headline5?.copyWith(fontSize: 12.0)),
                  Text("Chewy Chou",
                      style:
                          theme.textTheme.headline5?.copyWith(fontSize: 24.0))
                ],
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
                border: Border.all(color: theme.colorScheme.primary),
                shape: BoxShape.circle),
            child: IconButton(
              icon: Icon(Icons.notifications_none_outlined,
                  color: theme.colorScheme.primary),
              onPressed: () {
                return;
              },
            ),
          )
        ],
      ),
    );
  }
}
