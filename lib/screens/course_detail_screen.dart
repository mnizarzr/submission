import 'package:flutter/material.dart';
import 'package:submission/app_theme_data.dart';
import 'package:submission/model/course.dart';

class CourseDetailScreen extends StatefulWidget {
  const CourseDetailScreen({Key? key, required this.course}) : super(key: key);

  final Course course;

  @override
  State<CourseDetailScreen> createState() => _CourseDetailScreenState();
}

class _CourseDetailScreenState extends State<CourseDetailScreen> {
  bool _isFavorite = false;

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var size = MediaQuery.of(context).size;

    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Stack(
                  children: [
                    SizedBox(
                      height: size.width - 40,
                      width: size.width - 40,
                      child: ClipRRect(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(24.0)),
                        child: Image.network(
                          widget.course.imageUrl,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Positioned(
                      top: 10,
                      left: 10,
                      child: Container(
                        padding: const EdgeInsets.all(3.0),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(50),
                          ),
                          color: Color.fromRGBO(255, 255, 255, 0.5),
                        ),
                        child: IconButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            icon: const Icon(Icons.arrow_back)),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(defaultPadding),
                  child: Column(
                    children: [
                      Text(widget.course.title,
                          style: theme.textTheme.headline5
                              ?.copyWith(fontSize: 24.0, color: darkPurple)),
                      Padding(
                        padding: const EdgeInsets.all(defaultPadding),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              children: [
                                Text("Duration",
                                    style: theme.textTheme.headline5
                                        ?.copyWith(color: Colors.grey)),
                                Text(widget.course.duration,
                                    style: theme.textTheme.headline5)
                              ],
                            ),
                            Column(
                              children: [
                                Text("Students",
                                    style: theme.textTheme.headline5
                                        ?.copyWith(color: Colors.grey)),
                                Text(widget.course.totalStudents.toString(),
                                    style: theme.textTheme.headline5)
                              ],
                            ),
                            IconButton(
                                onPressed: () {
                                  setState(() {
                                    _isFavorite = !_isFavorite;
                                  });
                                },
                                icon: (() {
                                  if (_isFavorite) {
                                    return const Icon(Icons.favorite,
                                        color: Colors.pinkAccent);
                                  } else {
                                    return const Icon(
                                      Icons.favorite_outline,
                                    );
                                  }
                                }()))
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(defaultPadding),
                        child: Text(widget.course.shortDescription,
                            style: theme.textTheme.bodyText1),
                      )
                    ],
                  ),
                ),
                Container(
                  alignment: Alignment.bottomCenter,
                  width: size.width * 0.8,
                  height: 80,
                  decoration: BoxDecoration(
                      color: theme.colorScheme.primary,
                      borderRadius:
                          const BorderRadius.all(Radius.circular(20.0))),
                  child: InkWell(
                    onTap: () {
                      return;
                    },
                    child: Center(
                        child: Text(
                      "Add to Cart",
                      style: theme.textTheme.headline5
                          ?.copyWith(fontSize: 24.0, color: Colors.white),
                    )),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
