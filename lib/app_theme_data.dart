import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

const defaultPadding = 20.0;
const primaryColor = Color(0xFF9c7af8);
const secondaryColor = Color(0xFF8763c2);
const darkPurple = Color(0xFF322942);

abstract class AppThemeData {
  static ThemeData applicationThemeData =
      ThemeData(textTheme: _textTheme, colorScheme: _colorScheme);

  static const ColorScheme _colorScheme = ColorScheme(
      primary: primaryColor,
      secondary: secondaryColor,
      brightness: Brightness.light,
      onError: Colors.white,
      onBackground: Color(0x0DFFFFFF),
      onSecondary: Color(0xFF322942),
      onPrimary: Colors.white,
      background: Colors.white,
      error: Colors.white,
      surface: Colors.white,
      onSurface: Color(0xFF241E30));

  static const _regular = FontWeight.w400;
  static const _semiBold = FontWeight.w600;
  static const _bold = FontWeight.w700;

  // static const _medium = FontWeight.w500;

  static final TextTheme _textTheme = TextTheme(
    headline5: GoogleFonts.poppins(fontWeight: _semiBold, fontSize: 16.0),
    headline6: GoogleFonts.poppins(fontWeight: _bold, fontSize: 16.0),
    bodyText1: GoogleFonts.quicksand(fontWeight: _bold, fontSize: 20.0),
    caption: GoogleFonts.quicksand(fontWeight: _semiBold, fontSize: 16.0),
    button: GoogleFonts.poppins(fontWeight: _semiBold, fontSize: 14.0),
  );
}
